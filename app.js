var express  = require('express');
var app      = express();
var path = require('path')
var mongoose = require('mongoose');
var logger = require('morgan');
var bodyParser = require('body-parser');
var cors = require('cors');
// var config = require('./config/config')
var passport = require('passport');
require('./config/passport.js')(passport);
mongoose.Promise = global.Promise;
var databaseConfig = require('./config/database');
// var router = require('./app/routes');
 
mongoose.connect(databaseConfig.url,{ useNewUrlParser: true });
 
// app.set('views', __dirname+"/views/test");
app.use(express.static(path.join(__dirname, 'public')));
app.use('/public', express.static(path.resolve(__dirname, 'public')));

app.use(express.static(__dirname + '/public'));
app.set('view engine','ejs');
app.use(bodyParser.urlencoded({ extended: true })); // Parses urlencoded bodies
app.use(bodyParser.json()); // Send JSON responses
app.use(logger('dev')); // Log requests to API using morgan
app.use(cors());
app.use(passport.initialize());
app.use(passport.session());

//  Add all .routes.js files you write here in such a manner
require('./app/routes/auth.routes.js')(app);
require('./app/routes/user.routes.js')(app);
require('./app/routes/browse.routes')(app);
require('./app/routes/mcq.routes.js')(app);
app.listen(port = process.env.PORT || 8080,()=>{
    console.log(`Listening on port ${port}`);
});