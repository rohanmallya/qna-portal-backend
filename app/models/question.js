var mongoose = require('mongoose');

var answerJSON = {
    answer: String,
    studentID :{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
    },

    studentName: {
        type:String,
        required:true
    },
    correct: Boolean
}

var QuestionSchema = new mongoose.Schema({
    studentID: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    correctMarked:{
        type:Boolean,
        default:false
    },
    studentName: {
        type:String,
        required: true
    },
    question: {
        type:String,
        required: true
    },
    votes: {
        type:Number,
        default:1
    },
    answers:[answerJSON],
    categories: [String]
});

module.exports = mongoose.model('Question', QuestionSchema);