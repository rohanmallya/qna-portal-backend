var mongoose = require('mongoose');

var CategorySchema = new mongoose.Schema({
   name: {
       type:String,
       unique: true,
       lowercase:true
   },
   number: {
       type:Number
   }
});

CategorySchema.index({name: 'text', 'name': 'text'});
module.exports = mongoose.model('Category', CategorySchema);