var mongoose = require('mongoose');
var answerJSON = {
    answer: {
        type:String,
        required:true
    },
    correct: {
        type:Boolean,
        required:true,
    }
}
var MCQSchema = new mongoose.Schema({
   question:{
       type:String,
       required:true
   },
   studentID: {
    type:mongoose.Schema.Types.ObjectId,
    ref:'User',
   },
   answers:[answerJSON],
   categories:[String],
   votes:{
       type:Number,
       default:1
   }
});

module.exports = mongoose.model('MCQ', MCQSchema);