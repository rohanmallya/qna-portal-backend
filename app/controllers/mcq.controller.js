var User = require("../models/user");
var Question = require('../models/mcq');
var Category = require("../models/category_mcq")

exports.view = async function (req, res, next) {
    var questions = await Question.find();
    return res.status(201).send(questions);
}


exports.add = async function (req,res,next){
    var question = req.body.question;
    var categories = req.body.categories;
    var answers = req.body.answers;
    var studentID = req.user.id;
    categories = categories.map(i => i.toLowerCase());
    var questionJSON = {
        question: question,
        studentID:studentID,
        categories: categories,
        answers:answers,
    }
    var newQuestion = new Question(questionJSON);
    await newQuestion.save();

    for (var i = 0; i < categories.length; i++) {
        var cat = categories[i];
        var category = await Category.findOne({
            'name': cat
        });
        if (category === null) {
            var c = new Category({name: cat, number: 1})
            await c.save();
        } else{
            category['number'] += 1;
            await category.save()
        }
    }
    return res.status(201).send({
        "message": "Saved, man!"
    })
}

exports.delete = async function (req,res,next){
    var questionID = req.body.questionId;
    var question = await Question.findById(questionID);
    var categories = question.categories;
    if (question == null) {
        res.status(422).send({
            "error": "Question not found!"
        })
    } else{
        if(question.studentID != req.body.id){
            return res.status(401).send({"error": "You did not create this question!"})
        }
        for(var i = 0; i < categories.length; i++){
            var c = await Category.findOne({name:categories[i]});
            c['number'] -= 1;
            await c.save();
            if(c['number'] <= 0){
                await Category.findByIdAndRemove(c['_id']);
            }
        }
    }
    await question.remove();
    res.status(201).send({
        "message": "Deleted Question"
    });
}

exports.upvote = async function (req, res, next) {
    var questionId = req.body.questionId;
    var question = await Question.findById(questionId);
    question.votes += 1;
    await question.save();
    return res.status(201).send({
        "message": "Upvoted!"
    })
}

exports.downvote = async function (req, res, next) {
    var questionId = req.body.questionId;
    var question = await Question.findById(questionId);
    question.votes -= 1;
    await question.save();
    return res.status(201).send({
        "message": "Downvoted!"
    })
}

exports.get = async function (req,res,next){
    var topic = req.params.topic;
    topic = (topic.split("+")).join(" ");
    var query = {
        "categories": {
            "$in": [topic]
        }
    }
    var questions = await Question.find(query).sort([['votes',-1]]);
    res.status(201).send(questions);
}

exports.getTopics = async function (req,res,next){
    var topics = await Category.find().sort([["number",-1]]).select('name -_id').limit(10)
    return res.status(201).send(topics)
}