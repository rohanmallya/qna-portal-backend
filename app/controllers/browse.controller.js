var User = require("../models/user");
var Question = require('../models/question');
var Category = require("../models/category")

exports.view = async function (req, res, next) {
    var questions = await Question.find();
    return res.status(201).send(questions);

}

exports.ask = async function (req, res, next) {
    var question = req.body.question;
    var studentID = req.user.id; // This is studentID
    var categories = req.body.categories;
    var student = await User.findById(studentID);
    var name = student['name'];
    categories = categories.map(i => i.toLowerCase());
    var questionJSON = {
        question: question,
        studentID: studentID,
        categories: categories,
        studentName: name
    }
    var newQuestion = new Question(questionJSON);
    console.log(newQuestion)
    await newQuestion.save();
    console.log("HERE")

    for (var i = 0; i < categories.length; i++) {
        var cat = categories[i];
        var category = await Category.findOne({
            'name': cat
        });
        if (category === null) {
            var c = new Category({name: cat, number: 1})
            await c.save();
        } else{
            category['number'] += 1;
            await category.save()
        }
    }
    return res.status(201).send({
        "message": "Saved, man!"
    })
}

exports.deleteQuestion = async function (req, res, next) {
    var studentID = req.user.id;
    var questionID = req.body.questionId;
    var question = await Question.findById(questionID);
    if (question == null) {
        res.status(422).send({
            "error": "Question not found!"
        })
    } else{
        var categories = question.categories;

        if(question['studentID'] != studentID){
            return  res.status(401).send({"error":"You did not create this question!"})
        }
        for(var i = 0; i < categories.length; i++){
            var c = await Category.findOne({name:categories[i]});
            c['number'] -= 1;
            await c.save();
            if(c['number'] <= 0){
                await Category.findByIdAndRemove(c['_id']);
            }
        }
    }
    await question.remove();
    res.status(201).send({
        "message": "Deleted Question"
    });
}

exports.correctAnswer = async function (req, res, next) {
    var questionID = req.body.questionId;
    var userId = req.user.id;
    var studentID = req.body.studentID;
    var question = await Question.findById(questionID);
    if(question['correctMarked']){
        return res.status(422).send({"error":"Correct Answer is already marked"})
    }
    else if(question['studentID'] != userId){
        return res.status(401).send({"error": "You did not ask this question!"})
    }
    var answers = question.answers;
    for (var i = 0; i < answers.length; i++) {
        if (answers[i].studentID == studentID) {
            answers[i].correct = true;
            question['correctMarked'] = true
            await question.save();
            return res.status(201).send({
                "message": "Answer marked correct!"
            })

        }
    }

}

exports.viewQuestion = async function (req, res, next) {
    var questionId = req.params.questionId;
    var question = await Question.findById(questionId);
    return res.status(201).send(question);
}

exports.upvote = async function (req, res, next) {
    var questionId = req.body.questionId;
    var question = await Question.findById(questionId);
    question.votes += 1;
    await question.save();
    return res.status(201).send({
        "message": "Upvoted!"
    })
}

exports.downvote = async function (req, res, next) {
    var questionId = req.body.questionId;
    var question = await Question.findById(questionId);
    question.votes -= 1;
    await question.save();
    return res.status(201).send({
        "message": "Downvoted!"
    })
}

exports.answer = async function (req, res, next) {
    var studentID = req.user.id;
    var student = await User.findById(studentID);
    var answer = req.body.answer;
    var questionID = req.body.questionId;
    var question = await Question.findById(questionID);
    var answers = question.answers;
    for (var i = 0; i < answers.length; i++) {
        if (answers[i].studentID == studentID) {
            return res.status(422).send({
                "error": "This user has already answered the question"
            })
        }
    }
    question.answers.push({
        answer: answer,
        studentName: student.name,
        studentID: studentID,
        correct: false
    });
    await question.save();
    res.status(201).send({
        "message": "Answered!"
    })
}

exports.deleteAnswer = async function (req, res, next) {
    var studentID = req.user.id;
    var questionId = req.body.questionId;
    var question = await Question.findById(questionId);
    var answers = question.answers;
    
    for (var i = 0; i < answers.length; i++) {
        if (answers[i].studentID == studentID) {
            
            if(answers[i].correct){
                question.correctMarked = false;
            }
            answers.splice(i, 1);
            await question.save();
            return res.status(201).send({
                "message": "Deleted!"
            })
        }
    }
}

exports.getTopics = async function (req, res, next) {
    var topic = req.params.topic;
    topic = (topic.split("+")).join(" ");
    var query = {
        "categories": {
            "$in": [topic]
        }
    }
    var questions = await Question.find(query).sort([['votes',-1]]);
    res.status(201).send(questions);
}

exports.search = async function (req,res,next){
    var name = req.body.name;
    var category = await Category.find({$text:{$search:name}});
    return res.status(201).send(category);
}