var authController = require('../controllers/auth.controller'),
    express = require('express'),
    passportService = require('../../config/auth');
    passport = require('passport');

var requireAuth = passport.authenticate('jwt',{session: false});
var requireLogin = passport.authenticate('local', {session:false});
module.exports = (app) => {
   app.post('/register',authController.register);
   app.post('/login',requireLogin,authController.login);
   app.get('/protec', requireAuth, function(req,res){
       res.send({content: "SUCCESS"});
   })
   app.get('/dummy',function(req,res){
       res.send("Yeah boi")
   })
}
