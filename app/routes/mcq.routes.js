var authController = require('../controllers/auth.controller'),
    express = require('express'),
    passportService = require('../../config/auth');
    passport = require('passport');

var requireAuth = passport.authenticate('jwt',{session: false});
var requireLogin = passport.authenticate('local', {session:false});
var mcqController = require('../controllers/mcq.controller');
module.exports = (app) => {
 
    app.post("/mcq/add",requireAuth,mcqController.add);
    app.post("/mcq/delete",requireAuth,mcqController.delete);
    // app.post("/mcq/edit",mcqController.edit);
    app.post("/mcq/upvote",requireAuth,mcqController.upvote);
    app.post("/mcq/downvote",requireAuth,mcqController.downvote);
    app.get("/mcq/topics",mcqController.getTopics)
    app.get("/mcq/:topic/get",mcqController.get);
}