var authController = require('../controllers/auth.controller'),
    express = require('express'),
    passportService = require('../../config/auth');
    passport = require('passport');

var requireAuth = passport.authenticate('jwt',{session: false});
var requireLogin = passport.authenticate('local', {session:false});
var browseController = require('../controllers/browse.controller');
module.exports = (app) => {
    // get
    app.get("/questions",requireAuth,browseController.view); // Tested
    app.post("/question/ask",requireAuth,browseController.ask); // Tested
    app.post("/question/delete",requireAuth,browseController.deleteQuestion); // TEsted
    app.post("/answer/correct",requireAuth,browseController.correctAnswer); // Tested
    // GET
    app.get("/question/:questionId",requireAuth,browseController.viewQuestion) // Tested
    app.post("/upvote",requireAuth,browseController.upvote) // Tested
    app.post("/downvote",requireAuth,browseController.downvote) // Tested
    app.post("/answer",requireAuth,browseController.answer); // Tested
    app.post("/answer/delete",requireAuth,browseController.deleteAnswer) //Tested
    // GET
    app.post("/topics/search",requireAuth,browseController.search);
    app.get("/topics/:topic",requireAuth,browseController.getTopics); //Tested

}